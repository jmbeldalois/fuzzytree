# -*- coding: utf-8 -*-
"""
Created on Thu Aug 14 10:19:16 2014

@author: hp
"""

from FuzzyVars import *
from FuzzyTree import *
import csv

#%%
def read_example():
    filename = "ExampleYuan.txt"
    
    # Creating and empty Fuzzy Set
    output = FuzzySet(Outlook = ["Rainy", "Cloudy", "Sunny"],
                      Temperature = ["Hot", "Mild", "Cool"],
                      Humidity = ["Humid","Normal"],
                      Wind = ["Windy", "Calm" ],
                      Plan = ["Swimming", "Volleyball", "WeigthLifting"])    
    
    # Reading the .csv file and inserting FuzzyValues in the Fuzzy set
    with open(filename, 'rb') as f:
        reader = csv.DictReader(f, delimiter=';', quoting=csv.QUOTE_NONNUMERIC)
        for r in reader:
            outlook = FuzzyValue(Rainy = r["Rainy"],
                               Cloudy = r["Cloudy"],
                               Sunny  = r["Sunny"] )
                               
            temperature = FuzzyValue(Hot = r["Hot"],
                                   Mild = r["Mild"],
                                   Cool = r["Cool"])
                                   
            humidity = FuzzyValue(Humid = r["Humid"],
                                Normal = r["Normal"])
                                
            wind = FuzzyValue(Windy = r["Windy"],
                            Calm = r["Not windy"])
                            
            plan = FuzzyValue(Swimming = r["Swimming"],
                            Volleyball = r["Volleyball"],
                            WeigthLifting = r["W lifting"])
                            
            output.append(Outlook = outlook,
                          Temperature = temperature,
                          Humidity = humidity,
                          Wind = wind,
                          Plan = plan)
        return output

#%% Reading the example from the article
example = read_example()

#%% Creating the FuzzyTree
FT = FuzzyTree(example,
               0.7,
               ["Outlook", "Temperature", "Humidity", "Wind"],
                "Plan")

output = FT.classify(example)