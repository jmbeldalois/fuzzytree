# -*- coding: utf-8 -*-
"""
Created on Fri Aug 15 13:02:35 2014

@author: hp
"""

from FuzzyTree.FuzzyVars import *
from FuzzyTree.FuzzyTree import *
from FuzzyTree.FT_optimize import *
from numpy import percentile, linspace
from matplotlib.pylab import plot, savefig, figure, ion, title, close
import csv


ion()

def read_data():
    data = dict()
    filename = "imus_w.txt"
    
    data["Sujeto"] = []
    data["Genero"] = []
    data["Velocidad"] = []
    data["Cadencia"] = []
    data["Desfase"] = []
    data["ControlPelvis"] = []

    data["Edad"] = []
    data["BBS"] = []
    data["BI"] = []
    data["BipHS"] = []
    data["CapHS"] = []
    data["FAC"] = []
    data["FACHS"] = []    
    data["TUG"] = []
    data["MMT"] = []
    data["MAS"] = []
    data["DiaAlta"] = []
    data["DiaIctus"] = []
    
    data["HipFE_Healthy"] = []
    data["KneFE_Healthy"] = []
    data["AnkleFE_Healthy"] = []

    data["HipAbd_Healthy"] = []          
    data["HipFE_Pathological"] = []
    data["KneFE_Pathological"] = []
    data["AnkleFE_Pathological"] = []
    data["HipAbd_Pathological"] = []
    
    with open(filename, 'rb') as f:
        reader = csv.DictReader(f, delimiter='\t')#, quoting=csv.QUOTE_NONNUMERIC)
        for rd in reader:
            data["Sujeto"].append(rd["Sujeto"])
            data["Genero"].append(rd["Gender"])
            data["Velocidad"].append(to_float(rd["Velocidad"]))
            data["Cadencia"].append(to_float(rd["Cadencia"]))            
            data["Desfase"].append(to_float(rd["Desfase"]))
            data["ControlPelvis"].append(to_float(rd["ControlPelvis"]))

            data["Edad"].append(to_float(rd["Age"]))            
            data["BBS"].append(to_float(rd["BBS"]))            
            data["BI"].append(to_float(rd["BI"]))            
            data["BipHS"].append(to_float(rd["BipHS"]))            
            data["CapHS"].append(to_float(rd["CapHS"]))            
            data["FAC"].append(to_float(rd["FAC"]))            
            data["FACHS"].append(to_float(rd["FACHS"]))            
            data["TUG"].append(to_float(rd["TUG"]))            
            data["MMT"].append(to_float(rd["MMT"]))            
            data["MAS"].append(to_float(rd["MAS"]))            
            data["DiaAlta"].append(to_float(rd["DiaAlta"]))
            data["DiaIctus"].append(to_float(rd["DiaIctus"]))
            
            data["HipFE_Healthy"].append(to_float(rd["HipFE.Healthy"]))
            data["KneFE_Healthy"].append(to_float(rd["KneFE.Healthy"]))
            data["AnkleFE_Healthy"].append(to_float(rd["AnkleFE.Healthy"]))

            data["HipAbd_Healthy"].append(to_float(rd["HipAbd.Healthy"]))            
            data["HipFE_Pathological"].append(to_float(rd["HipFE.Pathological"]))            
            data["KneFE_Pathological"].append(to_float(rd["KneFE.Pathological"]))            
            data["AnkleFE_Pathological"].append(to_float(rd["AnkleFE.Pathological"]))            
            data["HipAbd_Pathological"].append(to_float(rd["HipAbd.Pathological"]))
            
        return data


def to_float(text):
    if text == "":
        return None
    try:
        val = float(text.replace(",","."))
    except(ValueError):
        val = None
        
    return val
    
    
#%% 
mis_datos = read_data()



#%% Vamos a tratar de Fuzzyficar en bloque
vars_in_set =    dict(Genero = ["Man", "Woman"],
                 Velocidad = ["Lenta", "Media","Rapida"],
                 #Velocidad = ["Lenta", "Rapida"],
                 Cadencia = ["Lenta", "Media","Rapida"],
                 #Cadencia = ["Bajo", "Alto"],
                 Desfase = ["Bajo", "Medio","Alto"],
                 ControlPelvis = ["Bajo", "Medio", "Alto"],
                 Edad = ["<50", "60",">70"],
                 #Edad = ["Joven", "Mayor"],
                 FACHS = ["<=3", ">=4"],
                 FAC = ["<=3", ">3"],        
                 MAS = ["<1", ">=1"],
                 MMT = ["<=3", ">4"],
                 #BBS = ["Bajo", "Medio","Alto"],
                 BI  = ["<80", "'85", "'90", "100"],
                 TUG = ["Bajo", "'Medio-", "'Medio+", "Alto"],
                 DiaAlta = ["<3 meses", "4", "5", "5+", "6"],
                 DiaIctus = ["<3 meses", "4", "5", "6"],
                 HipFE_Healthy = ["Bajo", "Medio", "Alto"],
                 HipFE_Pathological = ["Bajo", "Medio", "Alto"],
                 KneFE_Healthy = ["Bajo", "Medio", "Alto"],
                 KneFE_Pathological = ["Bajo", "Medio", "Alto"],
                 AnkleFE_Healthy = ["Bajo", "Medio", "Alto"],
                 AnkleFE_Pathological = ["Bajo", "Medio", "Alto"],
                 HipAbd_Healthy = ["Bajo", "Medio", "Alto"],
                 #HipAbd_Pathological = ["Bajo", "Medio", "Alto"]
                 HipAbd_Pathological = ["Bajo", "Alto"]
                )

# Variables que funcionan como entradas
LHS = ["Genero", "Velocidad", "Cadencia", "Desfase", "ControlPelvis",
       "Edad", "HipFE_Healthy", "HipFE_Pathological", "KneFE_Healthy",
       "KneFE_Pathological", "AnkleFE_Healthy", "AnkleFE_Pathological",
       "HipAbd_Pathological"]#,"MAS"]
       
RHS = "TUG"
Beta= 0.85
Alfa = 0.5
       
Entradas = set(LHS)

Salidas = ["BI", "FAC", "FACHS", "MMT", "MAS", "DiaAlta", "DiaIctus", "TUG"]

                
#%% Funciones de fuzzyficacion                
FzVars = []

# Fuzzyficamos la variable de salida
#fzFunc, fzO_Var = percentile_partition(mis_datos[RHS], RHS, vars_in_set[RHS])

#FzVars.append(fzO_Var)
    
# Fuzzyficamos las variables de entrada
for v in vars_in_set:
    if v == "Genero":
        fzFunc, Var  = crisp_partition(mis_datos[v],v,vars_in_set[v])
    else:
        #fzFunc, Var = optimize_partition(fzO_Var,mis_datos[v],v,vars_in_set[v])
        fzFunc, Var = percentile_partition(mis_datos[v],v,vars_in_set[v])
        
        # Dibujamos las funciones de pertenencia
#        a = figure()
#        mini = min(mis_datos[v])
#        if mini == None : mini = percentile(mis_datos[v],2)
#        fzFunc.do_plot(linspace(mini,max(mis_datos[v]),800))
#        title(v)
#        savefig("./figs/"+v+".png", bbox_inches='tight')
#        close(a)
                                     
    FzVars.append(Var)
                            


datos = FuzzySet(*FzVars)

the_dir = "./dot/"



#%% Inferimos el arbol del clasificacion
for RHS in Salidas:
    filename = "./Trees/" + RHS + ".txt"
    f = open(filename, "w")
    
    #Creating the fuzzy tree
    FT = FuzzyTree(datos, Beta, Alfa, LHS, RHS)
    
    #Confusion matrix
    cnfmtx, txtmtx = FT.confussion_matrix(datos[RHS],datos)
    
    #Output to a file
    print >> f, FT
    print >> f, txtmtx
    the_file = the_dir + RHS + ".dot"
    FT.output_to_dot_graphviz(the_file)
