# -*- coding: utf-8 -*-
"""
Created on Thu Jun 12 14:32:48 2014

@author: JMBELDA
"""

from FuzzyTree.FuzzyVars import *
from FuzzyTree.FuzzyTree import *
import csv

#%%
def read_example():
    filename = "ExampleYuan.txt"
    
    # Creating and empty Fuzzy Set
    output = FuzzySet(Outlook = ["Rainy", "Cloudy", "Sunny"],
                      Temperature = ["Hot", "Mild", "Cool"],
                      Humidity = ["Humid","Normal"],
                      Wind = ["Windy", "Calm" ],
                      Plan = ["Swimming", "Volleyball", "WeigthLifting"])    
    
    # Reading the .csv file and inserting FuzzyValues in the Fuzzy set
    with open(filename, 'rb') as f:
        reader = csv.DictReader(f, delimiter=';', quoting=csv.QUOTE_NONNUMERIC)
        for r in reader:
            outlook = FuzzyValue(Rainy = r["Rainy"],
                               Cloudy = r["Cloudy"],
                               Sunny  = r["Sunny"] )
                               
            temperature = FuzzyValue(Hot = r["Hot"],
                                   Mild = r["Mild"],
                                   Cool = r["Cool"])
                                   
            humidity = FuzzyValue(Humid = r["Humid"],
                                Normal = r["Normal"])
                                
            wind = FuzzyValue(Windy = r["Windy"],
                            Calm = r["Not windy"])
                            
            plan = FuzzyValue(Swimming = r["Swimming"],
                            Volleyball = r["Volleyball"],
                            WeigthLifting = r["W lifting"])
                            
            output.append(Outlook = outlook,
                          Temperature = temperature,
                          Humidity = humidity,
                          Wind = wind,
                          Plan = plan)
        return output

#%% Reading the example from the article
example = read_example()

#%% Calculating the ambiguity of the output
print "Plan Ambiguity:", example["Plan"].ambiguity()

#%% Calculating the vagueness of Temperature:Mild
print "Temperature Mild vagueness:", example["Temperature"]["Mild"].vagueness()

#%% Using some (Fuzzy)logic
sunny = example.mu("Outlook:Sunny")
cloudy = example.mu("Outlook:Cloudy")
rainy = example.mu("Outlook","Rainy")
hot = (example["Temperature"] == "Hot")
cool = (example["Temperature"] == "Cool")
swimming = example.mu("Plan:Swimming")
w_lifting = example.mu("Plan:WeigthLifting")

print (sunny | cloudy) & swimming
print (sunny & hot)


#%% Some subsethood calculations
print "IF Sunny & Hot THEN Swimming", sunny & hot <= swimming
print "IF Rainy & Cold THEN W lifting", subsethood(rainy & cool, w_lifting)

#%% Example of partitioning
plan = example["Plan"]
volleyball = (plan == "Volleyball")
swimming = (plan == "Swimming")
w_lifting = (plan == "WeigthLifting")

subsethood(hot, volleyball)
subsethood(hot, swimming)
subsethood(hot, w_lifting)

#%% Fuzzy evidence
print "Fuzzy evidence", FuzzyEvidence(plan, hot)
print "Classification ambiguity on Fuzzy evidence", FuzzyEvidence(plan, hot).ambiguity()

#%% Fuzzy evidence with partitioning
outlook = example["Outlook"]
print "Classification ambiguity with Fuzzy partition", ClassAmbiguityWithP(plan,outlook,hot)


#%% Creating the FuzzyTree
FT = FuzzyTree(example,
               0.7,
               0.5,
               ["Outlook", "Temperature", "Humidity", "Wind"],
                "Plan")
#%%
print FT

print FT.classify(example)

FT.confussion_matrix(example["Plan"],example)

#%% Let's do some fuzzyfication
low = cFF(lff,0.,0.5)
medium = cFF(cff,0.,0.5,1.)
high = cFF(rff,0.5,1.)

a = Fuzzification("my_var", low = low, medium = medium, high = high)

ud = linspace(-0.5,1.5,150)
other_var = a(ud)
#res = map(a,ud)
#
#other_var = FuzzyVar("my_var",low = [], medium =[], high = [])
#for r in res:
#    other_var.append(**r)
    
plot(ud,other_var["low"])
plot(ud,other_var["medium"])
plot(ud,other_var["high"])

