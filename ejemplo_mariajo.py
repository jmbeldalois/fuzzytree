# -*- coding: utf-8 -*-
"""
Created on Fri Aug 15 13:02:35 2014

@author: hp
"""

from FuzzyTree.FuzzyVars import *
from FuzzyTree.FuzzyTree import *
from FuzzyTree.FT_optimize import *
from numpy import percentile, linspace
from matplotlib.pylab import plot, savefig, figure, ion, title, close
import csv


ion()






def read_data():
    data = dict()
    filename = "TesisAfecto.csv"
    
    data_floats = ["edad",
                  "TALTA","DIAS_R","DIAS_I","DIAS","trhb",
                  "Height","Weight","ThightLength","LegLenth",
                  "FACHS_F3",
                  "FACHS_F5",
                  "FAC_F5",
                  "FAC_F3",
                  "BBS",
                  "BI",
                  "BipHS",
                  "CapHS",
                  "CNSParte_A","CNSParte_B","CNSTotal",
                  "EVA",
                  "FAC",
                  "FACHS",
                  "MASCuadriceps","MASPsoas","MASTibial_anterior",
                  "MMTCuadriceps","MMTPsoas","MMTTibial_anterior",
                  "SedHS",
                  "TCT",
                  "TUG",
                  "Velocidad",
                  "Tiempo_apoyo",
                  "Coefs_Fz1","Coefs_Fz2","Coefs_Fz3","Coefs_Fz4",
                  "Coefs_Fy1","Coefs_Fy2","Coefs_Fy3","Coefs_Fy4",
                  "Coefs_Fx1","Coefs_Fx2","Coefs_Fx3","Coefs_Fx4"]
                  
                  
    data_floats = set(data_floats)

    
    with open(filename, 'rb') as f:
        reader = csv.DictReader(f, delimiter=',')#, quoting=csv.QUOTE_NONNUMERIC)
        for rd in reader:
            for k in rd.keys():
                if k in data_floats:
                    temp = to_float(rd[k])
                else:
                    temp = rd[k]
                    
                try:
                    data[k].append(temp)
                except(KeyError):
                    data[k] = [temp]
            
        return data


def to_float(text):
    if text == "":
        return None
    try:
        val = float(text.replace(",","."))
    except(ValueError):
        val = None
        
    return val
    
    
#%% 
mis_datos = read_data()


#%% Vamos a tratar de Fuzzyficar en bloque
vars_in_set =    dict(edad = ["Joven", "Adulto", "Mayor"],
                Gender = ["Hombre", "Mujer"],
                Side = ["Izquierda", "Derecha"],
                TALTA = ["Agudo", "Subagudo", "Cronico"],
                DIAS_R = ["Agudo", "Subagudo", "Cronico"],
                DIAS_I = ["Agudo", "Subagudo", "Cronico"],
                DIAS = ["Agudo", "Subagudo", "Cronico"],
                trhb = ["Bajo", "Medio", "Alto"],
                Height = ["Bajo", "Medio", "Alto"],
                Weight = ["Bajo", "Medio", "Alto"],
                ThightLength = ["Bajo", "Medio", "Alto"],
                LegLenth = ["Bajo", "Medio", "Alto"],
                FACHS_F3 = ["Bajo", "Medio", "Alto"],
                FACHS_F5 = ["Bajo", "Medio", "Alto"],
                FAC_F5 = ["Bajo", "Medio", "Alto"],
                #FAC_F3 = ["Bajo", "Medio", "Alto"],
                BI = ["Bajo", "Medio", "Alto"],
                FAC = ["Bajo", "Medio", "Alto"],
                FACHS = ["Bajo", "Medio", "Alto"],
                TUG = ["Bajo", "Medio", "Alto"],
                Velocidad = ["Lenta", "Media", "Rápida"],
                Tiempo_apoyo = ["Bajo", "Medio", "Alto"],
                Coefs_Fz1 = ["Bajo", "Medio", "Alto"],
                Coefs_Fz2 = ["Bajo", "Medio", "Alto"],
                Coefs_Fz3 = ["Bajo", "Medio", "Alto"],
                Coefs_Fz4 = ["Bajo", "Medio", "Alto"],
                Coefs_Fy1 = ["Bajo", "Medio", "Alto"],
                Coefs_Fy2 = ["Bajo", "Medio", "Alto"],
                Coefs_Fy3 = ["Bajo", "Medio", "Alto"],
                Coefs_Fy4 = ["Bajo", "Medio", "Alto"],
                Coefs_Fx1 = ["Bajo", "Medio", "Alto"],
                Coefs_Fx2 = ["Bajo", "Medio", "Alto"],
                Coefs_Fx3 = ["Bajo", "Medio", "Alto"],
                Coefs_Fx4 = ["Bajo", "Medio", "Alto"]
                )

# Variables que funcionan como entradas
LHS = ["Coefs_Fz1", "Coefs_Fz2", "Coefs_Fz3", "Coefs_Fz4",
       "Coefs_Fx1", "Coefs_Fx2", "Coefs_Fx3", "Coefs_Fx4",
       "Coefs_Fy1", "Coefs_Fy2", "Coefs_Fy3", "Coefs_Fy4",
       "edad", "Side", #"Height", "Weight", "ThightLength", "LegLenth",
       #"FACHS_F3", "FACHS_F5", "FAC_F5",
       "Velocidad", "Tiempo_apoyo"]#,"MAS"]
       
vars_crisp = set(["Gender", "Side"])
       
Beta= 0.8
Alfa = 0.5
       
Entradas = set(LHS)

Salidas = ["TALTA", "DIAS_R", "DIAS_I", "DIAS", "TUG", "FACHS"]


#%% Helper function to find the real minimum
def real_min(values):
    new_v = [v for v in values]
    
    new_v.sort()
    
    for v in new_v:
        if v != None:
            break
        
    return v

                
#%% Funciones de fuzzyficacion                
FzVars = []

# Fuzzyficamos la variable de salida
#fzFunc, fzO_Var = percentile_partition(mis_datos[RHS], RHS, vars_in_set[RHS])

#FzVars.append(fzO_Var)
    
# Fuzzyficamos las variables de entrada
for v in vars_in_set:
    if v in vars_crisp:
        fzFunc, Var  = crisp_partition(mis_datos[v],v,vars_in_set[v])
    else:
        #fzFunc, Var = optimize_partition(fzO_Var,mis_datos[v],v,vars_in_set[v])
        fzFunc, Var = percentile_partition(mis_datos[v],v,vars_in_set[v])
        
        # Dibujamos las funciones de pertenencia
        a = figure()
        mini = min(mis_datos[v])
        if mini == None :
            print v
            mini = real_min(mis_datos[v])
        fzFunc.do_plot(linspace(mini,max(mis_datos[v]),800))
        title(v)
        savefig("./figs/"+v+".png", bbox_inches='tight')
        close(a)
                                     
    FzVars.append(Var)
                            


datos = FuzzySet(*FzVars)

the_dir = "./dot/"



#%% Inferimos el arbol del clasificacion
for RHS in Salidas:
    filename = "./Trees/" + RHS + ".txt"
    f = open(filename, "w")
    
    #Creating the fuzzy tree
    FT = FuzzyTree(datos, Beta, Alfa, LHS, RHS)
    
    #Confusion matrix
    cnfmtx, txtmtx = FT.confussion_matrix(datos[RHS],datos)
    
    #Output to a file
    print >> f, FT
    print >> f, txtmtx
    the_file = the_dir + RHS + ".dot"
    FT.output_to_dot_graphviz(the_file)
